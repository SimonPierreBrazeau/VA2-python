import sys
from datetime import datetime

class FPSCounter:
    def __init__ (self):
        self.startTime = 0
        self.endTime = 0
        self.frameTimes = 0
        self.frames = 0
        self.threshold = 1000
        self.lastLog = 0

    def startCounter (self):
        time = datetime.now()
        self.startTime = time.timestamp() * 1000

    def reset (self):
        self.frames = 0
        self.frameTimes = 0
        self.startCounter()

    def getFPS (self):
        return self.lastLog

    def logFPS (self):
        print(str(self.lastLog) + " fps")

    def stopAndPost (self, printLog):
        if self.startTime == 0:
            self.startCounter()
            return -1
        else:
            time = datetime.now()
            self.endTime = time.timestamp() * 1000
            self.frameTimes = self.endTime - self.startTime
            self.frames += 1

            if self.frameTimes >= self.threshold:
                self.lastLog = self.frames / (self.threshold / 1000)
                if printLog:
                    self.logFPS()
                self.reset()
                return self.lastLog
            return -1

