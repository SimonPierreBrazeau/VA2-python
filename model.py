class model:
    def __init__ (self):
        print("Generated model")
        self.vertices = []
        self.center = [0, 0]
        self.cvertices = []
        self.edges = []
        self.colors = []
        self.widths = []

    def addVertex (self, x, y):
        self.vertices.append((x, y))
        self.cvertices.append([0, 0])

    def addEdge (self, v1, v2, color, width):
        self.edges.append((v1, v2))
        self.colors.append(color)
        self.widths.append(width)
