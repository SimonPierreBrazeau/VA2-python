################################################
#                    Renderer                  #
################################################
import pygame
from pygame.locals import *
import math

class renderer:
    def __init__ (self):
        print("Renderer initialized")

    def render (self, obj, surface):
        for e in range(len(obj.edges)):
            startPoint = obj.cvertices[obj.edges[e][0]]
            endPoint = obj.cvertices[obj.edges[e][1]]
            color = obj.colors[e]
            self.drawLine(surface, startPoint, endPoint, color) 

    def drawLine (self, surface, start, finish, color):
        pygame.draw.line(surface, color, start, finish)

    def updateVertices (self, model, x, y, scale, rotation = 0, gui = False):
        model.center[0] = x
        model.center[1] = y

        for i in range(len(model.vertices)):
            vx = model.vertices[i][0]
            vy = model.vertices[i][1]

            # Scale vertices
            vx *= scale
            vy *= scale

            # Rotate vertives
            rx = math.cos(rotation * math.pi) * vx - math.sin(rotation * math.pi) * vy
            ry = math.sin(rotation * math.pi) * vx + math.cos(rotation * math.pi) * vy

            # Move things
            rx += x
            ry += y

            # Apply adjustments
            model.cvertices[i][0] = rx
            model.cvertices[i][1] = ry

