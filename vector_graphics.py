import pygame
import math
import time
from pygame.locals import *
from renderer import *
from model import *
from fps_counter import *
from decimal import *

####### CONSTANTS #######
iNANOSEC_MULTI = 1000000000
iTICK_LENGTH = (1 / 60) * iNANOSEC_MULTI

def Now ():
    return Decimal(time.time()) * iNANOSEC_MULTI

print("====== GAME STARTED ======")
pygame.init()
fps = FPSCounter()

width = 800
height = 600
clock = pygame.time.Clock()

screen = pygame.display.set_mode((width, height))
r = renderer()              # Create instance of renderer

####### TIMING STUFF #######
fLag = 0
dTimeStart = Now()

####### TEST MODEL #######
testModel = model()
testModel.addVertex(-1, -1)
testModel.addVertex(-1, 1)
testModel.addVertex(1, -1)

testModel.addEdge(0, 1, Color(255, 0, 255, 255), 1)
testModel.addEdge(1, 2, Color(255, 0, 255, 255), 1)
testModel.addEdge(2, 0, Color(255, 0, 255, 255), 1)

rot = 0    # Rotation of test model

fps.startCounter()          # Start FPS counter
iTempTick = 0
iTempTickCount = 0
while 1:
    ####### TIMING UPDATE #######
    fTimeDelta = float(Now() - dTimeStart)
    dTimeStart = Now()

    fLag += fTimeDelta

    ####### INPUTS #######
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            print("====== GAME STOPPED ======")
            pygame.quit()
            exit(0)

    ####### UPDATES #######
    while fLag >= iTICK_LENGTH:
        fLag = fLag - iTICK_LENGTH
        iTimeDeltaSec = fTimeDelta / iNANOSEC_MULTI
        rot += 0.005

    ####### RENDER #######
    # Clear the screen
    screen.fill(0)

    r.render(testModel, screen) 
    r.updateVertices(testModel, 200, 200, 100, rot)

    # Update screen
    pygame.display.flip()

    fps.stopAndPost(True)
